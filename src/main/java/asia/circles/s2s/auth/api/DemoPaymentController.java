package asia.circles.s2s.auth.api;

import asia.circles.s2s.auth.model.PaymentResponseDTO;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
@RequestMapping(DemoPaymentController.URL)
public class DemoPaymentController {

  public final static String URL = "/payments";


  private final RestTemplate restTemplate;

  @GetMapping
  public PaymentResponseDTO auth() {
    return new PaymentResponseDTO(UUID.randomUUID().toString());

  }
}
