package asia.circles.s2s.auth.api;

import asia.circles.s2s.auth.infrastructure.adapter.feignclient.PaymentConnectorApi;
import asia.circles.s2s.auth.model.PaymentRequestDTO;
import asia.circles.s2s.auth.model.PaymentResponseDTO;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
@RequiredArgsConstructor
@RequestMapping(DemoController.URL)
public class DemoController {

  public final static String URL = "/v1";

  private final PaymentConnectorApi paymentConnectorApi;

  private final RestTemplate restTemplate;

  private final WebClient webClient;

  @GetMapping
  public String inBound() {
    /**
     * This endpoint is authenticated
     */
    return "s2s Authenticated Endpoint!";
  }

  @PostMapping
  public void outBound() {
    /**
     * Just for the demo purpose of the outbound s2s calls
     */
    // FEIGN CLIENT
    paymentConnectorApi.createPayment(new PaymentRequestDTO());

    // REST TEMPLATE
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(List.of(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<>(headers);
    var response = restTemplate.exchange("http://localhost:8080/payments", HttpMethod.GET, entity,
        String.class).getBody();

    // WEB CLIENT
    var paymentResponseDTO = webClient
        .get()
        .uri("http://localhost:8080/payments")
        .retrieve()
        .bodyToMono(PaymentResponseDTO.class)
        .block();

  }
}
