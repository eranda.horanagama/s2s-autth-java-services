package asia.circles.s2s.auth.api.interceptors;

public class AuthException extends RuntimeException{

  public AuthException() {
    super();
  }

  public AuthException(String message) {
    super(message);
  }
}
