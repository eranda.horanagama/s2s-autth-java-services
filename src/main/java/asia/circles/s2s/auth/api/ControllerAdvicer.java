package asia.circles.s2s.auth.api;

import asia.circles.s2s.auth.api.interceptors.AuthException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerAdvicer {

  @ExceptionHandler(AuthException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public Map<String, Object> handle(AuthException e) {
    return new HashMap<>() {{
      put("status", "failure");
      put("error", e.getMessage());
    }};
  }
}
