package asia.circles.s2s.auth.api.interceptors;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AuthTokenFilter implements Filter {

  private final static String API_KEY = "API-KEY";

  @Value("${service.api-key}")
  private String configApiKey;

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                       FilterChain filterChain) throws IOException, ServletException {

    HttpServletRequest request = (HttpServletRequest) servletRequest;

    var apiKey = request.getHeader(API_KEY);
    if (apiKey == null || apiKey.isEmpty()) {
      throw new AuthException(String.format("%s not present in the request", API_KEY));
    }
    if (!apiKey.equals(configApiKey)) {
      throw new AuthException(String.format("Invalid %s in header", API_KEY));
    }
    // Do other validations as required - URL pattern etc..

    filterChain.doFilter(servletRequest, servletResponse);

  }
}
