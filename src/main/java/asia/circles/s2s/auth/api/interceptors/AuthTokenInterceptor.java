package asia.circles.s2s.auth.api.interceptors;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class AuthTokenInterceptor implements HandlerInterceptor {

  private final static String API_KEY = "API-KEY";

  @Value("${service.api-key}")
  private String configApiKey;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {

    var apiKey = request.getHeader(API_KEY);
    if (apiKey == null || apiKey.isEmpty()) {
      throw new AuthException(String.format("%s not present in the request", API_KEY));
    }
    if(!apiKey.equals(configApiKey)){
      throw new AuthException(String.format("Invalid %s in header", API_KEY));
    }
    // Do other validations as required - URL pattern etc..

    return true;
  }

  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                         ModelAndView modelAndView) throws Exception {
    HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                              Object handler, Exception ex) throws Exception {
    HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
  }
}
