package asia.circles.s2s.auth.config;

import asia.circles.s2s.auth.infrastructure.adapter.resttemplate.RestTemplateInterceptor;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class AppConfig {

  private final static String API_KEY_HEADER = "API-KEY";

  @Value("${payment.service.api-key}")
  private String externalServiceApiKey;

  @Bean
  public RestTemplate restTemplate() {
    RestTemplate restTemplate = new RestTemplate();

    List<ClientHttpRequestInterceptor> interceptors
        = restTemplate.getInterceptors();
    if (CollectionUtils.isEmpty(interceptors)) {
      interceptors = new ArrayList<>();
    }
    interceptors.add(new RestTemplateInterceptor(externalServiceApiKey));
    restTemplate.setInterceptors(interceptors);

    return restTemplate;
  }

  @Bean
  public WebClient webClient(){
    return WebClient.builder()
        .defaultHeader(API_KEY_HEADER,externalServiceApiKey)
        .build();
  }
}
