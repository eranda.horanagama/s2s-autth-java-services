package asia.circles.s2s.auth.infrastructure.adapter.resttemplate;

import java.io.IOException;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class RestTemplateInterceptor implements ClientHttpRequestInterceptor {

  private final String externalServiceApiKey;
  private final static String API_KEY_HEADER = "API-KEY";

  public RestTemplateInterceptor(String externalServiceApiKey) {
    this.externalServiceApiKey = externalServiceApiKey;
  }

  @Override
  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
                                      ClientHttpRequestExecution execution) throws IOException {
    request.getHeaders().add(API_KEY_HEADER, externalServiceApiKey);
    return execution.execute(request, body);
  }
}
