package asia.circles.s2s.auth.infrastructure.adapter.feignclient;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;

public class FeignClientConfig {

  @Bean
  public RequestInterceptor requestInterceptor() {
    return requestTemplate -> {
      requestTemplate.header("Content-Type", "application/json");
      requestTemplate.header("Accept", "application/json");
      //requestTemplate.header("Client-Id", clientId);
    };
  }

}
