package asia.circles.s2s.auth.infrastructure.adapter.feignclient;

import asia.circles.s2s.auth.model.PaymentRequestDTO;
import asia.circles.s2s.auth.model.PaymentResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "postFeignClient", configuration = FeignClientConfig.class, url = "${external.service.url}")
public interface PaymentConnectorApi {

  @PostMapping("v1/sg/en/ep/easypaisa/internal/payment/upfront")
  PaymentResponseDTO createPayment(PaymentRequestDTO paymentRequestDTO);
}
