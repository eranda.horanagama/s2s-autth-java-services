package asia.circles.s2s.auth.infrastructure.adapter.feignclient;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import java.util.Collections;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FeignInterceptor implements RequestInterceptor {

  private final static String API_KEY_HEADER = "API-KEY";
  @Value("${payment.service.api-key}")
  private String externalServiceApiKey;

  @Override
  public void apply(RequestTemplate requestTemplate) {

    requestTemplate.header(API_KEY_HEADER, Collections.singleton(externalServiceApiKey));
  }
}
