package asia.circles.s2s.auth;

import asia.circles.s2s.auth.api.interceptors.AuthTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.handler.MappedInterceptor;

@SpringBootApplication
@EnableAutoConfiguration
@EnableFeignClients
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public MappedInterceptor getMappedInterceptor(AuthTokenInterceptor authTokenInterceptor) {
		return new MappedInterceptor(new String[] { "/**" }, authTokenInterceptor);
	}
}
